%dw 1.0
%output application/java
---
[{
	airlineName: "American",
	code1: "5576",
	code2: "pxhjs",
	fromAirport: "CRD",
	planeType: "Boeing 747",
	price: 450,
	seatsAvailable: "20",
	takeOffDate: |2015-10-01|,
	toAirport: "PDX"
}]